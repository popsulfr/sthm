#ifndef STHM_H
#define STHM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#ifndef STHM_MAX_KEY_SIZE
#define STHM_MAX_KEY_SIZE 1024
#endif
#ifdef STHM_KEY_SIZE
#undef STHM_MAX_KEY_SIZE
#define STHM_MAX_KEY_SIZE (STHM_KEY_SIZE - 1)
#endif

#ifndef STHM_ARRAY_SIZE
#define STHM_ARRAY_SIZE 32
#endif

#ifndef STHMDEF
#ifdef STHM_STATIC
#define STHMDEF static
#else
#define STHMDEF extern
#endif
#endif

typedef struct {
#ifdef STHM_KEY_SIZE
	char k[STHM_KEY_SIZE];
#else
	const char *k;
#endif
#ifdef STHM_VALUE_SIZE
	char v[STHM_VALUE_SIZE];
#else
	const void *v;
#endif
} sthm_node;

typedef struct {
	unsigned int n;
	sthm_node c[STHM_ARRAY_SIZE];
} sthm;

STHMDEF void sthm_init(sthm *s);
STHMDEF bool sthm_put(sthm *s, const char *key, const void *value);
STHMDEF bool sthm_remove(sthm *s, const char *key);
STHMDEF const void* sthm_get(const sthm *s, const char *key);

#ifdef __cplusplus
}
#endif

#endif /* STHM_H */

#ifdef STHM_IMPLEMENTATION

#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>

STHMDEF inline unsigned int sthm_djb2(const char *s, unsigned int n) {
	unsigned int hash = 5381;
	for (; n && *s; n--, s++) hash = ((hash << 5) + hash) ^ (unsigned int)toupper(*s);
	return hash;
}

STHMDEF void sthm_init(sthm *s) {
	memset(s, 0, sizeof(sthm));
}

STHMDEF bool sthm_put(sthm *s, const char *key, const void *value) {
	if (!key || !*key) return false;
	unsigned int i = sthm_djb2(key, STHM_MAX_KEY_SIZE) % STHM_ARRAY_SIZE;
	unsigned int c = 0;
	sthm_node *n = &s->c[i];
	for (; c < STHM_ARRAY_SIZE &&
#ifdef STHM_KEY_SIZE
		*n->k
#else
		n->k
#endif
		&& strncasecmp(key, n->k, STHM_MAX_KEY_SIZE); c++, i = (i + 1) % STHM_ARRAY_SIZE, n = &s->c[i]);
	if (c == STHM_ARRAY_SIZE) return false;
#ifdef STHM_KEY_SIZE
	if (!*n->k) {
		strncpy(n->k, key, sizeof(n->k));
		n->k[sizeof(n->k) - 1] = 0;
		s->n++;
	}
#else
	if (!n->k) {
		n->k = key;
		s->n++;
	}
#endif
#ifdef STHM_VALUE_SIZE
	memcpy(n->v, value, STHM_VALUE_SIZE);
#else
	n->v = value;
#endif
	return true;
}

STHMDEF bool sthm_remove(sthm *s, const char *key) {
	if (!s->n || !key || !*key) return false;
	unsigned int i = sthm_djb2(key, STHM_MAX_KEY_SIZE) % STHM_ARRAY_SIZE;
	unsigned int c = 0;
	sthm_node *n = &s->c[i];
	for (; c < s->n &&
#ifdef STHM_KEY_SIZE
		*n->k
#else
		n->k
#endif
		&& strncasecmp(key, n->k, STHM_MAX_KEY_SIZE); c++, i = (i + 1) % STHM_ARRAY_SIZE, n = &s->c[i]);
	if (c == s->n ||
#ifdef STHM_KEY_SIZE
		!*n->k
#else
		!n->k
#endif
		) return false;
	memset(n, 0, sizeof(sthm_node));
	s->n--;
	unsigned int di = 0;
	unsigned int li = i;
	sthm_node *ni;
	for (i = (i + 1) % STHM_ARRAY_SIZE, ni = &s->c[i];
#ifdef STHM_KEY_SIZE
		*ni->k
#else
		ni->k
#endif
			&& di < STHM_ARRAY_SIZE - (c + 1); di++, i = (i + 1) % STHM_ARRAY_SIZE, ni = &s->c[i]) {
		unsigned int hi = sthm_djb2(ni->k, STHM_MAX_KEY_SIZE) % STHM_ARRAY_SIZE;
		if (hi == i) continue;
		unsigned int dhi = (i + STHM_ARRAY_SIZE - hi) % STHM_ARRAY_SIZE;
		unsigned int dli = (li + STHM_ARRAY_SIZE - hi) % STHM_ARRAY_SIZE;
		if (dli > dhi) continue;
		memcpy(n, ni, sizeof(sthm_node));
		memset(ni, 0, sizeof(sthm_node));
		n = ni;
		li = i;
	}
	return true;
}

STHMDEF const void* sthm_get(const sthm *s, const char *key) {
	if (!s->n || !key || !*key) return NULL;
	unsigned int i = sthm_djb2(key, STHM_MAX_KEY_SIZE) % STHM_ARRAY_SIZE;
	unsigned int c = 0;
	const sthm_node *n = &s->c[i];
	for (; c < s->n &&
#ifdef STHM_KEY_SIZE
		*n->k
#else
		n->k
#endif
		&& strncasecmp(key, n->k, STHM_MAX_KEY_SIZE); c++, i = (i + 1) % STHM_ARRAY_SIZE, n = &s->c[i]);
	if (c == s->n ||
#ifdef STHM_KEY_SIZE
		!*n->k
#else
		!n->k
#endif
		) return NULL;
	return n->v;
}

#endif /* STHM_IMPLEMENTATION */
