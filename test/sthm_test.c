#include "unity.h"

#define MAX_KEY_SIZE 32
#define MAX_ARRAY_SIZE 128

#define STHM_KEY_SIZE MAX_KEY_SIZE
#define STHM_ARRAY_SIZE MAX_ARRAY_SIZE
#define STHM_STATIC
#define STHM_IMPLEMENTATION
#include "sthm.h"

#include <ctype.h>

static inline uint64_t xoroshiro128p_rotl(const uint64_t x, int k) {
	return (x << k) | (x >> (64 - k));
}

static uint64_t xoroshiro128p_s[2] = {(uint64_t)&xoroshiro128p_s, (uint64_t)&xoroshiro128p_rotl};

static uint64_t xoroshiro128p_next(void) {
	const uint64_t s0 = xoroshiro128p_s[0];
	uint64_t s1 = xoroshiro128p_s[1];
	const uint64_t result = s0 + s1;

	s1 ^= s0;
	xoroshiro128p_s[0] = xoroshiro128p_rotl(s0, 24) ^ s1 ^ (s1 << 16); // a, b
	xoroshiro128p_s[1] = xoroshiro128p_rotl(s1, 37); // c

	return result;
}

void gen_string(char *buf, size_t size) {
	for (; size > 1; buf++, size--) {
		*buf = 32 + (xoroshiro128p_next() % (126 - 32));
	}
	*buf = 0;
}

sthm S;

void setUp(void) {
	// set stuff up here
	sthm_init(&S);
}

void tearDown(void) {
	// clean stuff up here
}

void test_sthm_should_map(void) {
	TEST_ASSERT_EQUAL_UINT(0, S.n);
	uint64_t values[MAX_ARRAY_SIZE];
	for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
		char buf[MAX_KEY_SIZE];
		gen_string(buf, 4 + (xoroshiro128p_next() % (MAX_KEY_SIZE - 4)));
		values[i] = xoroshiro128p_next();
		TEST_PRINTF("Key: '%s', Value: %u\n", buf, values[i]);
		TEST_ASSERT_TRUE(sthm_put(&S, buf, &values[i]));
		TEST_ASSERT_EQUAL_UINT(i + 1, S.n);
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, buf));
		TEST_ASSERT_EQUAL_UINT(values[i], *(uint64_t*)sthm_get(&S, buf));
		TEST_ASSERT_TRUE(sthm_remove(&S, buf));
		TEST_ASSERT_EQUAL_UINT(i, S.n);
		TEST_ASSERT_NULL(sthm_get(&S, buf));
		TEST_ASSERT_TRUE(sthm_put(&S, buf, &values[i]));
	}
	TEST_ASSERT_EQUAL_UINT(MAX_ARRAY_SIZE, S.n);
}

void test_sthm_should_map2(void) {
	TEST_ASSERT_EQUAL_UINT(0, S.n);
	char keys[MAX_ARRAY_SIZE][MAX_KEY_SIZE];
	uint64_t values[MAX_ARRAY_SIZE];
	for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
		gen_string(keys[i], 4 + (xoroshiro128p_next() % (MAX_KEY_SIZE - 4)));
		values[i] = xoroshiro128p_next();
		TEST_PRINTF("[%d] Put Key: '%s', Value: %u\n", i, keys[i], values[i]);
		TEST_ASSERT_TRUE(sthm_put(&S, keys[i], &values[i]));
		TEST_ASSERT_EQUAL_UINT(i + 1, S.n);
		TEST_PRINTF("[%d] Get Key: '%s'\n", i, keys[i]);
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
	}
	TEST_ASSERT_EQUAL_UINT(MAX_ARRAY_SIZE, S.n);
	for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
	}
	for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
		TEST_PRINTF("[%d] Remove Key: '%s'\n", i, keys[i]);
		TEST_ASSERT_TRUE(sthm_remove(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT(MAX_ARRAY_SIZE - (i + 1), S.n);
		*keys[i] = 0;
		for (int j = 0; j < MAX_ARRAY_SIZE; j++) {
			if (!*keys[j]) continue;
			TEST_PRINTF("[%d] Get Key: '%s'\n", j, keys[j]);
			TEST_ASSERT_EQUAL_PTR_MESSAGE(&values[j], sthm_get(&S, keys[j]), keys[j]);
			TEST_ASSERT_EQUAL_UINT64(values[j], *(uint64_t*)sthm_get(&S, keys[j]));
		}
	}
	TEST_ASSERT_EQUAL_UINT(0, S.n);
}

void test_sthm_should_map3(void) {
	TEST_ASSERT_EQUAL_UINT(0, S.n);
	char keys[MAX_ARRAY_SIZE][MAX_KEY_SIZE];
	uint64_t values[MAX_ARRAY_SIZE];
	for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
		gen_string(keys[i], 4 + (xoroshiro128p_next() % (MAX_KEY_SIZE - 4)));
		values[i] = xoroshiro128p_next();
		TEST_PRINTF("[%d] Put Key: '%s', Value: %u\n", i, keys[i], values[i]);
		TEST_ASSERT_TRUE(sthm_put(&S, keys[i], &values[i]));
		TEST_ASSERT_EQUAL_UINT(i + 1, S.n);
		TEST_PRINTF("[%d] Get Key: '%s'\n", i, keys[i]);
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
	}
	TEST_ASSERT_EQUAL_UINT(MAX_ARRAY_SIZE, S.n);
	for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
	}
	for (int i = 0; i < MAX_ARRAY_SIZE; ) {
		int r = xoroshiro128p_next() % MAX_ARRAY_SIZE;
		if (!*keys[r]) continue;
		TEST_PRINTF("[%d] Remove Key: '%s'\n", r, keys[r]);
		TEST_ASSERT_TRUE(sthm_remove(&S, keys[r]));
		TEST_ASSERT_EQUAL_UINT(MAX_ARRAY_SIZE - (i + 1), S.n);
		*keys[r] = 0;
		for (int j = 0; j < MAX_ARRAY_SIZE; j++) {
			if (!*keys[j]) continue;
			TEST_PRINTF("[%d] Get Key: '%s'\n", j, keys[j]);
			TEST_ASSERT_EQUAL_PTR_MESSAGE(&values[j], sthm_get(&S, keys[j]), keys[j]);
			TEST_ASSERT_EQUAL_UINT64(values[j], *(uint64_t*)sthm_get(&S, keys[j]));
		}
		i++;
	}
}

void test_sthm_should_map_nocase(void) {
	TEST_ASSERT_EQUAL_UINT(0, S.n);
	char keys[MAX_ARRAY_SIZE][MAX_KEY_SIZE];
	uint64_t values[MAX_ARRAY_SIZE];
	for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
		gen_string(keys[i], 4 + (xoroshiro128p_next() % (MAX_KEY_SIZE - 4)));
		values[i] = xoroshiro128p_next();
		TEST_PRINTF("[%d] Put Key: '%s', Value: %u\n", i, keys[i], values[i]);
		TEST_ASSERT_TRUE(sthm_put(&S, keys[i], &values[i]));
		TEST_ASSERT_EQUAL_UINT(i + 1, S.n);
		TEST_PRINTF("[%d] Get Key: '%s'\n", i, keys[i]);
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
		for (char *c = keys[i]; *c; c++) *c = toupper(*c);
		TEST_PRINTF("[%d] Get Key: '%s'\n", i, keys[i]);
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
		TEST_PRINTF("[%d] Put Key: '%s', Value: %u\n", i, keys[i], values[i]);
		TEST_ASSERT_TRUE(sthm_put(&S, keys[i], &values[i]));
		TEST_ASSERT_EQUAL_UINT(i + 1, S.n);
		TEST_PRINTF("[%d] Get Key: '%s'\n", i, keys[i]);
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
		for (char *c = keys[i]; *c; c++) *c = tolower(*c);
		TEST_PRINTF("[%d] Get Key: '%s'\n", i, keys[i]);
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
		TEST_PRINTF("[%d] Put Key: '%s', Value: %u\n", i, keys[i], values[i]);
		TEST_ASSERT_TRUE(sthm_put(&S, keys[i], &values[i]));
		TEST_ASSERT_EQUAL_UINT(i + 1, S.n);
		TEST_PRINTF("[%d] Get Key: '%s'\n", i, keys[i]);
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
	}
	TEST_ASSERT_EQUAL_UINT(MAX_ARRAY_SIZE, S.n);
	for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
		TEST_ASSERT_EQUAL_PTR(&values[i], sthm_get(&S, keys[i]));
		TEST_ASSERT_EQUAL_UINT64(values[i], *(uint64_t*)sthm_get(&S, keys[i]));
	}
	for (int i = 0; i < MAX_ARRAY_SIZE; ) {
		int r = xoroshiro128p_next() % MAX_ARRAY_SIZE;
		if (!*keys[r]) continue;
		TEST_PRINTF("[%d] Remove Key: '%s'\n", r, keys[r]);
		TEST_ASSERT_TRUE(sthm_remove(&S, keys[r]));
		TEST_ASSERT_EQUAL_UINT(MAX_ARRAY_SIZE - (i + 1), S.n);
		*keys[r] = 0;
		for (int j = 0; j < MAX_ARRAY_SIZE; j++) {
			if (!*keys[j]) continue;
			TEST_PRINTF("[%d] Get Key: '%s'\n", j, keys[j]);
			TEST_ASSERT_EQUAL_PTR_MESSAGE(&values[j], sthm_get(&S, keys[j]), keys[j]);
			TEST_ASSERT_EQUAL_UINT64(values[j], *(uint64_t*)sthm_get(&S, keys[j]));
		}
		i++;
	}
}

int main(void) {
	UNITY_BEGIN();
	//RUN_TEST(test_sthm_should_map);
	RUN_TEST(test_sthm_should_map2);
	RUN_TEST(test_sthm_should_map3);
	RUN_TEST(test_sthm_should_map_nocase);
	return UNITY_END();
}
